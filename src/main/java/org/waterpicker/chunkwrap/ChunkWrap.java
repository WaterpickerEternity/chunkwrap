package org.waterpicker.chunkwrap;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.WorldType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.Logger;
import org.waterpicker.chunkwrap.worldgen.CustomizedPlanetWorldType;
import org.waterpicker.chunkwrap.worldgen.EventListener;

@Mod(modid = ChunkWrap.MODID, name = ChunkWrap.NAME, version = ChunkWrap.VERSION)
@Mod.EventBusSubscriber
public class ChunkWrap {

    public static final String MODID = "chunkwrap";
    public static final String NAME = "ChunkWrap";
    public static final String VERSION = "@@VERSION@@";

    public static Logger LOGGER;

    public static WorldType customizedPlanet = new CustomizedPlanetWorldType();

    private static final SimpleNetworkWrapper dispatcher = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        LOGGER = event.getModLog();

        MinecraftForge.TERRAIN_GEN_BUS.register(new EventListener());
        MinecraftForge.EVENT_BUS.register(new EventListener());

        dispatcher.registerMessage(PacketWorldSize.Handler.class, PacketWorldSize.class, 0, Side.CLIENT);
    }

    @SubscribeEvent
    public static void onPlayerJoinWorld(EntityJoinWorldEvent evt) {
        if (evt.getEntity() instanceof EntityPlayerMP) {
            IWrappedWorld world = (IWrappedWorld) evt.getWorld();
            dispatcher.sendTo(new PacketWorldSize(world.getSizeXBits(), world.getSizeZBits()), (EntityPlayerMP) evt.getEntity());
        }
    }
}
