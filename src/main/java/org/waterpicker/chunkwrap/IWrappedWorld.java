package org.waterpicker.chunkwrap;

import org.waterpicker.chunkwrap.util.Coords;

public interface IWrappedWorld {

    void initSizeBits(int bitsX, int bitsZ);

    int getSizeXBits();

    int getSizeZBits();

    default int getSizeXBlocks() {
        return 1 << getSizeXBits();
    }

    default int getSizeZBlocks() {
        return 1 << getSizeZBits();
    }

    default int getSizeXChunks() {
        return Coords.blockToChunk(getSizeXBlocks());
    }

    default int getSizeZChunks() {
        return Coords.blockToChunk(getSizeZBlocks());
    }
}
