package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.chunk.CompiledChunk;
import net.minecraft.client.renderer.chunk.RenderChunk;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.waterpicker.chunkwrap.render.IWrappedRenderChunk;

@Mixin(RenderGlobal.class)
public class MixinRenderGlobal {

    @Redirect(method = {"setupTerrain", "renderBlockLayer"},
            at = @At(value = "INVOKE", target = "Lnet/minecraft/client/renderer/chunk/RenderChunk;getCompiledChunk()"
                    + "Lnet/minecraft/client/renderer/chunk/CompiledChunk;"))
    private CompiledChunk getCompiledChunkForSetupTerrain(RenderChunk rc) {
        if (((IWrappedRenderChunk) rc).isWrapped()) {
            return ((IWrappedRenderChunk) rc).getWrapped().getCompiledChunk();
        }
        return rc.getCompiledChunk();
    }
}
