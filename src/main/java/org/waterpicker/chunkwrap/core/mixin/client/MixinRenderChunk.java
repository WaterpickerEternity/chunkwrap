package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.chunk.RenderChunk;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.waterpicker.chunkwrap.util.Coords;

@Mixin(RenderChunk.class)
public abstract class MixinRenderChunk {

    @Shadow @Final private BlockPos.MutableBlockPos position;

    @Shadow public abstract void stopCompileTask();

    @Shadow public AxisAlignedBB boundingBox;

    @Shadow @Final private BlockPos.MutableBlockPos[] mapEnumFacing;

    @Shadow protected abstract void initModelviewMatrix();

    @Shadow private World world;

    /**
     * @author Barteks2x
     */
    @Overwrite
    public void setPosition(int x, int y, int z) {
        if (Coords.wrapBlockX(world, x) != Coords.wrapBlockX(world, this.position.getX())
                || y != this.position.getY()
                || Coords.wrapBlockZ(world, z) != Coords.wrapBlockZ(world, this.position.getZ())) {
            if (!Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
                this.stopCompileTask();
            }
        }
        if (x != position.getX() || y != position.getY() || z != position.getZ()) {
            this.position.setPos(x, y, z);
            this.boundingBox = new AxisAlignedBB((double) x, (double) y, (double) z, (double) (x + 16), (double) (y + 16), (double) (z + 16));
            for (EnumFacing enumfacing : EnumFacing.values()) {
                this.mapEnumFacing[enumfacing.ordinal()].setPos(this.position).move(enumfacing, 16);
            }

            this.initModelviewMatrix();
        }
    }

}
