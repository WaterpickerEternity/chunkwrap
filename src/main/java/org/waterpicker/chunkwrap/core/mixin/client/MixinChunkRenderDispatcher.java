package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.chunk.ChunkRenderDispatcher;
import net.minecraft.client.renderer.chunk.RenderChunk;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.waterpicker.chunkwrap.render.IWrappedRenderChunk;

@Mixin(ChunkRenderDispatcher.class)
public class MixinChunkRenderDispatcher {

    @Inject(method = {"updateChunkLater", "updateChunkNow", "updateTransparencyLater"}, at = @At("HEAD"), cancellable = true)
    private void checkIsWrapped(RenderChunk orig, CallbackInfoReturnable<Boolean> ci) {
        if (((IWrappedRenderChunk) orig).isWrapped()) {
            ci.setReturnValue(true);
            ci.cancel();
        }
    }
}
