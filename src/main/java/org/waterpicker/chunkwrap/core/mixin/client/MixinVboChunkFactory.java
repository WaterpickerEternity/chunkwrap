package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.chunk.RenderChunk;
import net.minecraft.client.renderer.chunk.VboChunkFactory;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.waterpicker.chunkwrap.render.WrappedRenderChunk;

@Mixin(VboChunkFactory.class)
public class MixinVboChunkFactory {
    /**
     * @author Barteks2x
     */
    @Overwrite
    public RenderChunk create(World worldIn, RenderGlobal renderGlobalIn, int index) {
        return new WrappedRenderChunk(worldIn, renderGlobalIn, index);
    }
}
