package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.ViewFrustum;
import net.minecraft.client.renderer.chunk.IRenderChunkFactory;
import net.minecraft.client.renderer.chunk.RenderChunk;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.waterpicker.chunkwrap.IWrappedWorld;
import org.waterpicker.chunkwrap.render.IWrappedRenderChunk;
import org.waterpicker.chunkwrap.util.Coords;

import javax.annotation.Nullable;

@Mixin(ViewFrustum.class)
public abstract class MixinViewFrustum {

    @Shadow public RenderChunk[] renderChunks;

    @Shadow @Final protected World world;

    @Shadow @Nullable protected abstract RenderChunk getRenderChunk(BlockPos pos);

    @Shadow protected int countChunksX;

    @Shadow protected int countChunksZ;

    @Shadow protected abstract int getBaseCoordinate(int p_178157_1_, int p_178157_2_, int p_178157_3_);

    @Shadow protected int countChunksY;

    private RenderChunk[] rcBuffer;
    private boolean firstUpdate = true;
    private double lastEntityX, lastEntityZ;

    @Inject(method = "createRenderChunks", at = @At(value = "RETURN"))
    private void onCreateRenderChunk(IRenderChunkFactory renderChunkFactory, CallbackInfo ci) {
        updateWrapStatus(countChunksX * 16 / 2, countChunksZ * 16 / 2);
        firstUpdate = true;
        this.rcBuffer = new RenderChunk[this.countChunksX * this.countChunksY * this.countChunksZ];
    }

    @Inject(method = "updateChunkPositions", at = @At(value = "RETURN"))
    private void onCreateRenderChunk(double viewEntityX, double viewEntityZ, CallbackInfo ci) {
        updateWrapStatus(viewEntityX, viewEntityZ);
    }

    @Inject(method = "updateChunkPositions", at = @At(value = "HEAD"))
    private void preUpdateChunkPositions(double viewEntityX, double viewEntityZ, CallbackInfo ci) {
        if (firstUpdate) {
            lastEntityX = viewEntityX;
            lastEntityZ = viewEntityZ;
            firstUpdate = false;
        }
        int worldSizeX = ((IWrappedWorld) world).getSizeXBlocks();
        int worldSizeZ = ((IWrappedWorld) world).getSizeZBlocks();
        if (!(Math.abs(lastEntityX - viewEntityX) + 64 > worldSizeX
                || Math.abs(lastEntityZ - viewEntityZ) + 64 > worldSizeZ)) {
            lastEntityX = viewEntityX;
            lastEntityZ = viewEntityZ;
            return;
        }

        RenderChunk[] oldArray = this.renderChunks;
        this.renderChunks = this.rcBuffer;
        this.rcBuffer = oldArray;

        int entityX = MathHelper.floor(viewEntityX) - 8;
        int entityZ = MathHelper.floor(viewEntityZ) - 8;

        int prevEntityX = MathHelper.floor(lastEntityX) - 8;
        int prevEntityZ = MathHelper.floor(lastEntityZ) - 8;


        int dx = entityX - prevEntityX;
        dx = (int) (Math.round(dx / (double) worldSizeX) * worldSizeX);

        int dz = entityZ - prevEntityZ;
        dz = (int) (Math.round(dz / (double) worldSizeZ) * worldSizeZ);

        int sizeXZ = this.countChunksX * 16;

        for (int xIdx = 0; xIdx < this.countChunksX; ++xIdx) {
            int xNew = this.getBaseCoordinate(entityX, sizeXZ, xIdx);
            int xOld = xNew - dx;

            for (int zIdx = 0; zIdx < this.countChunksZ; ++zIdx) {
                int zNew = this.getBaseCoordinate(entityZ, sizeXZ, zIdx);
                int zOld = zNew - dz;

                for (int yIdx = 0; yIdx < this.countChunksY; ++yIdx) {
                    int y = yIdx * 16;

                    int oldIdx = getIdx(xOld, y, zOld);
                    int newIdx = getIdx(xNew, y, zNew);
                    renderChunks[newIdx] = oldArray[oldIdx];
                }
            }
        }

        lastEntityX = viewEntityX;
        lastEntityZ = viewEntityZ;
    }

    private int getIdx(int x, int y, int z) {
        int i = MathHelper.intFloorDiv(x, 16);
        int j = MathHelper.intFloorDiv(y, 16);
        int k = MathHelper.intFloorDiv(z, 16);

        i = i % this.countChunksX;

        if (i < 0) {
            i += this.countChunksX;
        }

        k = k % this.countChunksZ;

        if (k < 0) {
            k += this.countChunksZ;
        }

        int l = (k * this.countChunksY + j) * this.countChunksX + i;
        return l;
    }

    private void updateWrapStatus(double viewEntityX, double viewEntityZ) {
        int minX = Integer.MAX_VALUE;
        int minZ = Integer.MAX_VALUE;
        for (RenderChunk renderChunk : renderChunks) {
            if (renderChunk.getPosition().getX() < minX) {
                minX = renderChunk.getPosition().getX();
            }
            if (renderChunk.getPosition().getZ() < minZ) {
                minZ = renderChunk.getPosition().getZ();
            }
        }
        for (RenderChunk renderChunk : renderChunks) {
            BlockPos pos = renderChunk.getPosition();
            BlockPos wrapped = new BlockPos(
                    Coords.wrapRelativeX(world, pos.getX(), minX),
                    pos.getY(),
                    Coords.wrapRelativeZ(world, pos.getZ(), minZ));
            if (getRenderChunk(pos) != getRenderChunk(wrapped)) {
                RenderChunk toWrap = getRenderChunk(wrapped);
                ((IWrappedRenderChunk) renderChunk).setBaseChunk(toWrap);
            } else {
                ((IWrappedRenderChunk) renderChunk).setBaseChunk(null);
            }
        }
    }
}
