package org.waterpicker.chunkwrap.core.mixin;

import net.minecraft.world.ChunkCache;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.waterpicker.chunkwrap.util.Coords;

@Mixin(ChunkCache.class)
public class MixinChunkCache {

    @Redirect(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;getChunk(II)Lnet/minecraft/world/chunk/Chunk;"))
    private Chunk getCHunk(World world, int x, int z) {
        return world.getChunk(Coords.wrapChunkX(world, x), Coords.wrapChunkZ(world, z));
    }
}
