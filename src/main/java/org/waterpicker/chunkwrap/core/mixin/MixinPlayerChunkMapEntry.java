package org.waterpicker.chunkwrap.core.mixin;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketUnloadChunk;
import net.minecraft.server.management.PlayerChunkMap;
import net.minecraft.server.management.PlayerChunkMapEntry;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import java.util.List;

import javax.annotation.Nullable;

@Mixin(PlayerChunkMapEntry.class)
public abstract class MixinPlayerChunkMapEntry {

    @Shadow @Final private List<EntityPlayerMP> players;

    @Shadow private long lastUpdateInhabitedTime;

    @Shadow @Final private PlayerChunkMap playerChunkMap;

    @Shadow private boolean sentToPlayers;

    @Shadow public abstract void sendToPlayer(EntityPlayerMP player);

    @Shadow @Nullable private Chunk chunk;

    @Shadow private boolean loading;

    @Shadow @Final private ChunkPos pos;

    @Shadow private Runnable loadedRunnable;

    /**
     * @author Barteks2x
     */
    @Overwrite
    public void addPlayer(EntityPlayerMP player) {
        if (this.players.isEmpty()) {
            this.lastUpdateInhabitedTime = this.playerChunkMap.getWorldServer().getTotalWorldTime();
        }

        boolean contains = this.players.contains(player);
        this.players.add(player);

        if (contains) {
            return;
        }
        if (this.sentToPlayers) {
            this.sendToPlayer(player);
            // chunk watch event - the chunk is ready
            net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new net.minecraftforge.event.world.ChunkWatchEvent.Watch(this.chunk, player));
        }
    }

    /**
     * @author Barteks2x
     */
    @Overwrite
    public void removePlayer(EntityPlayerMP player) {
        if (this.players.contains(player)) {
            // If we haven't loaded yet don't load the chunk just so we can clean it up
            if (this.chunk == null) {
                this.players.remove(player);
                if (this.players.contains(player)) {
                    return;
                }

                if (this.players.isEmpty()) {
                    if (this.loading) {
                        net.minecraftforge.common.chunkio.ChunkIOExecutor
                                .dropQueuedChunkLoad(this.playerChunkMap.getWorldServer(), this.pos.x, this.pos.z, this.loadedRunnable);
                    }
                    this.playerChunkMap.removeEntry((PlayerChunkMapEntry) (Object) this);
                }

                return;
            }
            this.players.remove(player);
            if (this.players.contains(player)) {
                return;
            }

            if (this.sentToPlayers) {
                player.connection.sendPacket(new SPacketUnloadChunk(this.pos.x, this.pos.z));
            }

            net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new net.minecraftforge.event.world.ChunkWatchEvent.UnWatch(this.chunk, player));

            if (this.players.isEmpty()) {
                this.playerChunkMap.removeEntry((PlayerChunkMapEntry) (Object) this);
            }
        }
    }

}
