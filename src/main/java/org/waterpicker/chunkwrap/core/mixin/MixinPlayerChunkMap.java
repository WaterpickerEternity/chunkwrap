package org.waterpicker.chunkwrap.core.mixin;

import net.minecraft.server.management.PlayerChunkMap;
import net.minecraft.world.WorldServer;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.waterpicker.chunkwrap.IWrappedWorld;
import org.waterpicker.chunkwrap.util.Coords;

@Mixin(PlayerChunkMap.class)
public abstract class MixinPlayerChunkMap {

    @Shadow @Final private WorldServer world;

    /**
     * @author Barteks2x
     */
    @Overwrite
    private boolean overlaps(int x1, int z1, int x2, int z2, int radius) {
        x1 = Coords.wrapChunkX(world, x1);
        z1 = Coords.wrapChunkZ(world, z1);

        x2 = Coords.wrapChunkX(world, x2);
        z2 = Coords.wrapChunkZ(world, z2);

        int i = Math.abs(x1 - x2);
        int j = Math.abs(z1 - z2);

        i = Math.min(i, ((IWrappedWorld) world).getSizeXChunks() - i);
        j = Math.min(j, ((IWrappedWorld) world).getSizeZChunks() - j);

        if (i >= -radius && i <= radius) {
            return j >= -radius && j <= radius;
        } else {
            return false;
        }
    }

    /**
     * @author Barteks2x
     */
    @Overwrite
    private static long getIndex(int chunkX, int chunkZ) {
        throw new Error("This method should never be used");
    }

    @Redirect(
            method = {"contains", "getEntry", "getOrCreateEntry", "removeEntry"},
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/server/management/PlayerChunkMap;getIndex(II)J"
            )
    )
    private long redirectGetIndex(int chunkX, int chunkZ) {
        chunkX = Coords.wrapChunkX(world, chunkX);
        chunkZ = Coords.wrapChunkZ(world, chunkZ);
        return (long) chunkX + 2147483647L | (long) chunkZ + 2147483647L << 32;
    }

    @ModifyVariable(
            method = "getOrCreateEntry",
            at = @At("HEAD"),
            index = 1,
            argsOnly = true
    )
    private int modifyXArgVar(int x) {
        return Coords.wrapChunkX(world, x);
    }

    @ModifyVariable(
            method = "getOrCreateEntry",
            at = @At("HEAD"),
            index = 2,
            argsOnly = true
    )
    private int modifyZArgVar(int z) {
        return Coords.wrapChunkZ(world, z);
    }
}
