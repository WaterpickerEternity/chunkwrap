package org.waterpicker.chunkwrap.core.mixin;

import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.waterpicker.chunkwrap.IWrappedWorld;

@Mixin(World.class)
public class MixinWorld implements IWrappedWorld {

    private int sizeXBits = 8, sizeZBits = 8;

    @Override public void initSizeBits(int bitsX, int bitsZ) {
        this.sizeXBits = bitsX;
        this.sizeZBits = bitsZ;
    }

    @Override public int getSizeXBits() {
        return sizeXBits;
    }

    @Override public int getSizeZBits() {
        return sizeZBits;
    }
}
