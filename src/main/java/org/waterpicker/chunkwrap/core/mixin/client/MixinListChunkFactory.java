package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.chunk.ListChunkFactory;
import net.minecraft.client.renderer.chunk.RenderChunk;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.waterpicker.chunkwrap.render.WrappedListedRenderChunk;

@Mixin(ListChunkFactory.class)
public class MixinListChunkFactory {
    /**
     * @author Barteks2x
     */
    @Overwrite
    public RenderChunk create(World worldIn, RenderGlobal renderGlobalIn, int index) {
        return new WrappedListedRenderChunk(worldIn, renderGlobalIn, index);
    }
}
