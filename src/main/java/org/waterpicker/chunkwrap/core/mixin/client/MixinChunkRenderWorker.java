package org.waterpicker.chunkwrap.core.mixin.client;

import net.minecraft.client.renderer.chunk.ChunkRenderWorker;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.waterpicker.chunkwrap.util.Coords;

@Mixin(ChunkRenderWorker.class)
public class MixinChunkRenderWorker {

    /**
     * @author Barteks2x
     */
    @Overwrite
    private boolean isChunkExisting(BlockPos pos, World worldIn) {
        return !worldIn.getChunk(Coords.wrapChunkX(worldIn, pos.getX() >> 4),
                Coords.wrapChunkZ(worldIn, pos.getZ() >> 4)).isEmpty();
    }
}
