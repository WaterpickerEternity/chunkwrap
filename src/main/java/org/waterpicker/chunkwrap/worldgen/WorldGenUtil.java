package org.waterpicker.chunkwrap.worldgen;

import net.minecraft.world.World;
import net.minecraft.world.gen.MapGenBase;
import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.GenLayerVoronoiZoom;
import net.minecraftforge.event.terraingen.InitNoiseGensEvent;
import net.minecraftforge.event.terraingen.InitNoiseGensEvent.ContextHell;
import net.minecraftforge.event.terraingen.InitNoiseGensEvent.ContextOverworld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.waterpicker.chunkwrap.ChunkWrap;
import org.waterpicker.chunkwrap.IWrappedWorld;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.GenLayerTileableUtil;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.TileableGenLayer;
import org.waterpicker.chunkwrap.worldgen.mapgen.MapGenTileable;
import org.waterpicker.chunkwrap.worldgen.noisegen.LoopedNoiseGenOctaves;
import org.waterpicker.chunkwrap.worldgen.noisegen.LoopedNoiseGenSimplex2d;
import org.waterpicker.chunkwrap.worldgen.util.Bits;

import java.util.List;
import java.util.Random;

public class WorldGenUtil {

    private static final Logger logger = LogManager.getLogger(ChunkWrap.MODID + ":"
            + WorldGenUtil.class.getSimpleName());

    public static void transformNoiseGenerators(InitNoiseGensEvent.Context oldGen, InitNoiseGensEvent.Context newGen, World world) {
        int dimId = world.provider.getDimension();
        logger.info("Replacing noise generator for world " + world.getWorldInfo().getWorldName() + ", dimensionID="
                + dimId);

        System.out.println("WorldClass" + world.getClass().getName());

		/*if (!oldGen.equals(newGen)) {
			throw new IllegalArgumentException("oldGen and newGen lengths are not equal!");
		}*/

        int loopBitsX = ((IWrappedWorld) world).getSizeXBits();
        int loopBitsZ = ((IWrappedWorld) world).getSizeZBits();

        newGen.setLPerlin1(new LoopedNoiseGenOctaves(oldGen.getLPerlin1(), world.getSeed(), loopBitsX, loopBitsZ));
        newGen.getLPerlin2(new LoopedNoiseGenOctaves(oldGen.getLPerlin2(), world.getSeed() + 1, loopBitsX, loopBitsZ));
        newGen.getPerlin(new LoopedNoiseGenOctaves(oldGen.getPerlin(), world.getSeed() + 2, loopBitsX, loopBitsZ));
        newGen.getScale(new LoopedNoiseGenOctaves(oldGen.getScale(), world.getSeed() + 3, loopBitsX, loopBitsZ));
        newGen.getDepth(new LoopedNoiseGenOctaves(oldGen.getDepth(), world.getSeed() + 4, loopBitsX, loopBitsZ));

        if (oldGen instanceof ContextOverworld && newGen instanceof ContextOverworld) {
            ((ContextOverworld) newGen)
                    .getHeight(new LoopedNoiseGenSimplex2d(((ContextOverworld) oldGen).getHeight(), world.getSeed() + 5, loopBitsX, loopBitsZ));
            ((ContextOverworld) newGen)
                    .getForest(new LoopedNoiseGenOctaves(((ContextOverworld) oldGen).getForest(), world.getSeed() + 6, loopBitsX, loopBitsZ));
        } else if (oldGen instanceof ContextHell && newGen instanceof ContextHell) {
        }
    }

    public static void transformGenLayers(GenLayer[] originalBiomeGens, GenLayer[] newBiomeGens, long seed) {
        logger.info("Replacing biome gen layers");

        for (int i = 0; i < originalBiomeGens.length; i++) {
            newBiomeGens[i] = GenLayerTileableUtil.getTileableLayer(originalBiomeGens[i]);
        }
    }

    public static void setLayerSize(List<GenLayer> layers, int sizeX, int sizeZ) {
        for (GenLayer layer : layers) {
            if (layer instanceof GenLayerVoronoiZoom) {
                ((TileableGenLayer) layer).setSizeBits(sizeX, sizeZ);
            } else {
                ((TileableGenLayer) layer).setSizeBits(sizeX - 2, sizeZ - 2);
            }
        }
    }

    public static void modifySeed(Random rand, int chunkX, int chunkZ, World world) {
        int bitmaskX = Bits.bitmaskClamped(((IWrappedWorld) world).getSizeXBits() - 4);
        int bitmaskZ = Bits.bitmaskClamped(((IWrappedWorld) world).getSizeZBits() - 4);
        chunkX &= bitmaskX;
        chunkZ &= bitmaskZ;
        long seed = world.getSeed();
        rand.setSeed(seed);
        long xRand = rand.nextLong() / 2L * 2L + 1L;
        long zRand = rand.nextLong() / 2L * 2L + 1L;
        rand.setSeed((long) chunkX * xRand + (long) chunkZ * zRand ^ seed);
    }

    public static MapGenBase tileableMapGen(MapGenBase originalGen) {
        logger.info("Creating new Generator for typr: " + originalGen.getClass());
        return new MapGenTileable(originalGen);
    }
}
