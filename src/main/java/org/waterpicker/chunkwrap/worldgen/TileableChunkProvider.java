package org.waterpicker.chunkwrap.worldgen;

import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.gen.ChunkGeneratorOverworld;
import net.minecraft.world.gen.layer.GenLayer;
import org.waterpicker.chunkwrap.IWrappedWorld;
import org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil;

import java.util.List;

public class TileableChunkProvider extends ChunkGeneratorOverworld {

    public TileableChunkProvider(World worldIn, long seed, boolean someFlag, String settings) {
        super(worldIn, seed, someFlag, getChunkProviderGenerateSettingsAndSetConfig(settings, worldIn));

        int sizeX = ((IWrappedWorld) worldIn).getSizeXBits();
        int sizeZ = ((IWrappedWorld) worldIn).getSizeZBits();

        BiomeProvider wcm = worldIn.provider.getBiomeProvider();
        List<GenLayer> genLayers = ReflectionUtil.getValuesByType(GenLayer.class, BiomeProvider.class, wcm, false);
        WorldGenUtil.setLayerSize(genLayers, sizeX, sizeZ);
    }

    private static String getTileableProviderSettings(String settings) {
        if (settings == null || settings.length() > 0 || settings.indexOf('|') == -1) {
            return null;
        }
        int end = settings.indexOf('|');
        String ret = settings.substring(0, end);
        return ret;
    }

    private static String getChunkProviderGenerateSettingsAndSetConfig(String settings, World world) {
        if (settings == null) {
            return null;
        }
        int start = settings.indexOf('|');
        String mySettings = getTileableProviderSettings(settings);
        // for now it will be just a single number
        int size = 0;
        try {
            size = Integer.parseInt(mySettings);
        } catch (NumberFormatException e) {
            size = 13;
        }
        ((IWrappedWorld) world).initSizeBits(size, size);
        String ret = settings.substring(start + 1);
        return ret;
    }

}
