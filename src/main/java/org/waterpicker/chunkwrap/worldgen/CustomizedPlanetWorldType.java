package org.waterpicker.chunkwrap.worldgen;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.gen.IChunkGenerator;

public class CustomizedPlanetWorldType extends WorldType {

    public CustomizedPlanetWorldType() {
        super("custom_planet");
    }

    @Override
    public void onCustomizeButton(Minecraft mc, GuiCreateWorld guiCreateWorld) {
        mc.displayGuiScreen(new CustomizedPlanetTypeGUI(guiCreateWorld));


    }

    @Override
    public IChunkGenerator getChunkGenerator(World world, String generatorOptions) {
        return new TileableChunkProvider(world, world.getSeed(), world.getWorldInfo()
                .isMapFeaturesEnabled(), generatorOptions);
    }

    public boolean isCustomizable() {
        return true;
    }

}
