package org.waterpicker.chunkwrap.worldgen.util;

public class ReflectionException extends RuntimeException {

    public ReflectionException(Exception ex) {
        super(ex);
    }
}
