package org.waterpicker.chunkwrap.worldgen;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.ChunkGeneratorOverworld;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.MapGenBase;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.terraingen.ChunkGeneratorEvent;
import net.minecraftforge.event.terraingen.InitNoiseGensEvent;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import net.minecraftforge.event.terraingen.WorldTypeEvent.InitBiomeGens;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.waterpicker.chunkwrap.ChunkWrap;
import org.waterpicker.chunkwrap.IWrappedWorld;
import org.waterpicker.chunkwrap.util.Coords;
import org.waterpicker.chunkwrap.util.Mappings;
import org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventListener {

    private static final Logger logger = LogManager
            .getLogger(ChunkWrap.MODID + ":" + EventListener.class.getSimpleName());

    private final Set<Integer> replacedMapGensDimensions = new HashSet<Integer>(3);

    @SubscribeEvent
    public void onEvent(InitNoiseGensEvent event) {
        WorldGenUtil.transformNoiseGenerators(event.getOriginal(), event.getNewValues(), event.getWorld());
    }

    @SubscribeEvent
    public void onEvent(InitBiomeGens event) {
        WorldGenUtil.transformGenLayers(event.getOriginalBiomeGens(), event.getNewBiomeGens(), event.getSeed());
    }

    @SubscribeEvent
    public void onEvent(PopulateChunkEvent.Pre event) {
        WorldGenUtil.modifySeed(event.getRand(), event.getChunkX(), event.getChunkZ(), event.getWorld());
    }

    @SubscribeEvent
    public void onEvent(ChunkGeneratorEvent.ReplaceBiomeBlocks event) {
        int dim = event.getWorld().provider.getDimension();
        if (this.replacedMapGensDimensions.contains(dim)) {
            // don't touch, we already replaced it
            return;
        }
        this.replacedMapGensDimensions.add(dim);
        IChunkGenerator prov = event.getGen();
        if (!(prov instanceof ChunkGeneratorOverworld)) {
            return;
        }
        List<Field> genFields = ReflectionUtil.getFieldsByType(MapGenBase.class, ChunkGeneratorOverworld.class, false);

        for (Field field : genFields) {
            MapGenBase value = ReflectionUtil.getValue(field, prov, MapGenBase.class);
            value = WorldGenUtil.tileableMapGen(value);
            ReflectionUtil.setValue(field, prov, value);
        }
    }

    @SubscribeEvent
    public void onEntityUpdate(LivingUpdateEvent event) {
        World world = event.getEntityLiving().getEntityWorld();
        if (!isDimensionBlacklisted(world.provider.getDimension())) {
            EntityLivingBase entity = event.getEntityLiving();
            Vec3d vec = new Vec3d(entity.posX, entity.posY, entity.posZ);
            if (shouldVec3dBeLooped(world, vec)) {
                Vec3d loopedPos = getLoopedVec3d(world, vec);
                double dx = loopedPos.x - vec.x; //  vec.x + dx == loopedPos.x
                double dz = loopedPos.z - vec.z;

                entity.setPosition(loopedPos.x, loopedPos.y, loopedPos.z);
                // this attempts to preserve continuity in movement
                // so that client rendering doesn't see any paused movement
                entity.prevPosX += dx;
                entity.prevPosZ += dz;
                entity.lastTickPosX += dx;
                entity.lastTickPosZ += dz;
            }
        }
    }

    @SubscribeEvent
    public void onChunkLoad(ChunkEvent.Load event) {
        World world = event.getWorld();
        if (world instanceof WorldServer && !isDimensionBlacklisted(world.provider.getDimension())) {
            Chunk chunk = event.getChunk();
            if (shouldChunkBeLooped(world, chunk)) {
                Chunk loopChunk = getLoopedChunk(world, chunk);
                Field[] fields = loopChunk.getClass().getDeclaredFields();
                for (Field f : fields) {
                    f.setAccessible(true);
                    Class t = f.getType();
                    try {
                        Field modifiersField = Field.class.getDeclaredField("modifiers");
                        modifiersField.setAccessible(true);
                        modifiersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);

                        // field_76635_g x | field_76647_h z
                        String fName = f.getName();
                        // Don't overwrite the x and y coordinates of the Chunk
                        if (!fName.equals(Mappings.getNameFromSrg("field_76635_g"))
                                && !fName.equals(Mappings.getNameFromSrg("field_76647_h"))) {
                            Object v = f.get(loopChunk);
                            f.set(chunk, v);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("hi there! Loop-loading chunk at: " + loopChunk.x + " , " + loopChunk.z);
            }
        }
    }

    public static boolean shouldChunkBeLooped(World world, Chunk chunk) {
        IWrappedWorld w = (IWrappedWorld) world;
        int minChunkX = 0;
        int minChunkZ = 0;
        int maxChunkX = w.getSizeXChunks();
        int maxChunkZ = w.getSizeZChunks();

        int chunkX = chunk.x;
        int chunkZ = chunk.z;

        if (minChunkX > chunkX || maxChunkX <= chunkX || minChunkZ > chunkZ || maxChunkZ <= chunkZ) {
            return true;
        }

        return false;
    }

    public static Chunk getLoopedChunk(World world, Chunk chunk) {
        return world.getChunk(Coords.wrapChunkX(world, chunk.x), Coords.wrapChunkZ(world, chunk.z));
    }

    public static boolean shouldVec3dBeLooped(World world, Vec3d pos) {
        IWrappedWorld w = (IWrappedWorld) world;
        double minX = 0;
        double minZ = 0;
        double maxX = w.getSizeXBlocks();
        double maxZ = w.getSizeZBlocks();

        double posX = pos.x;
        double posZ = pos.z;

        if (minX > posX || maxX < posX || minZ > posZ || maxZ < posZ) {
            return true;
        }

        return false;
    }

    public static Vec3d getLoopedVec3d(World world, Vec3d pos) {
        return new Vec3d(Coords.wrapEntityX(world, pos.x), pos.y, Coords.wrapEntityZ(world, pos.z));
    }

    public static boolean isDimensionBlacklisted(int dimensionId) {
        return false;
    }
}
