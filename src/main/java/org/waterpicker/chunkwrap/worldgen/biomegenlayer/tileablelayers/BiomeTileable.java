package org.waterpicker.chunkwrap.worldgen.biomegenlayer.tileablelayers;

import net.minecraft.world.gen.ChunkGeneratorSettings;
import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.GenLayerBiome;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.GenLayerTileableUtil;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.TileableGenLayer;
import org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil;

import java.util.List;

public class BiomeTileable extends GenLayerBiome implements TileableGenLayer {

    private int sizeX;
    private int sizeZ;

    public BiomeTileable(GenLayerBiome originalLayer) {
        super(0, null, null, null);
        ReflectionUtil.copyNonStaticFieldsByType(GenLayer.class, long.class, originalLayer, this);
        ReflectionUtil.copyNonStaticFieldsByType(GenLayer.class, GenLayer.class, originalLayer, this);
        ReflectionUtil.copyNonStaticFieldsByType(GenLayerBiome.class, List[].class, originalLayer, this);
        ReflectionUtil.copyNonStaticFieldsByType(GenLayerBiome.class, ChunkGeneratorSettings.class, originalLayer, this);
        this.parent = GenLayerTileableUtil.getTileableLayer(this.parent);
    }

    public void initChunkSeed(long x, long z) {
        super.initChunkSeed(x & sizeX, z & sizeZ);
    }

    @Override
    public void setSizeBits(int bitsX, int bitsZ) {
        this.sizeX = GenLayerTileableUtil.sizeBitmask(bitsX);
        this.sizeZ = GenLayerTileableUtil.sizeBitmask(bitsZ);
        TileableGenLayer tileable = (TileableGenLayer) this.parent;
        tileable.setSizeBits(bitsX, bitsZ);
    }
}
