package org.waterpicker.chunkwrap.worldgen.biomegenlayer.tileablelayers;

import static org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil.copyNonStaticFieldsByType;

import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.GenLayerEdge;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.GenLayerTileableUtil;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.TileableGenLayer;

public class EdgeTemperaturesTileable extends GenLayerEdge implements TileableGenLayer {

    private int sizeX;
    private int sizeZ;

    public EdgeTemperaturesTileable(GenLayerEdge original) {
        super(0, null, null);
        copyNonStaticFieldsByType(GenLayer.class, long.class, original, this);
        copyNonStaticFieldsByType(GenLayer.class, GenLayer.class, original, this);
        copyNonStaticFieldsByType(GenLayerEdge.class, Mode.class, original, this);

        this.parent = GenLayerTileableUtil.getTileableLayer(this.parent);
    }

    public void initChunkSeed(long x, long z) {
        super.initChunkSeed(x & sizeX, z & sizeZ);
    }

    @Override
    public void setSizeBits(int bitsX, int bitsZ) {
        ((TileableGenLayer) this.parent).setSizeBits(bitsX, bitsZ);
        this.sizeX = GenLayerTileableUtil.sizeBitmask(bitsX);
        this.sizeZ = GenLayerTileableUtil.sizeBitmask(bitsZ);
    }
}
