package org.waterpicker.chunkwrap.worldgen.biomegenlayer.tileablelayers;

import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.GenLayerBiomeEdge;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.GenLayerTileableUtil;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.TileableGenLayer;
import org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil;

public class BiomeEdgeTileable extends GenLayerBiomeEdge implements TileableGenLayer {

    private int sizeX;
    private int sizeZ;

    public BiomeEdgeTileable(GenLayerBiomeEdge originalLayer) {
        super(0, null);
        ReflectionUtil.copyNonStaticFieldsByType(GenLayer.class, long.class, originalLayer, this);
        ReflectionUtil.copyNonStaticFieldsByType(GenLayer.class, GenLayer.class, originalLayer, this);
        this.parent = GenLayerTileableUtil.getTileableLayer(this.parent);
    }

    public void initChunkSeed(long x, long z) {
        super.initChunkSeed(x & sizeX, z & sizeZ);
    }

    @Override
    public void setSizeBits(int bitsX, int bitsZ) {
        this.sizeX = GenLayerTileableUtil.sizeBitmask(bitsX);
        this.sizeZ = GenLayerTileableUtil.sizeBitmask(bitsZ);
        TileableGenLayer tileable = (TileableGenLayer) this.parent;
        tileable.setSizeBits(bitsX, bitsZ);
    }
}
