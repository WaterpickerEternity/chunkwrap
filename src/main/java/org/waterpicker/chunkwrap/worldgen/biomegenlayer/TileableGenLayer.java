package org.waterpicker.chunkwrap.worldgen.biomegenlayer;

public interface TileableGenLayer {

    void setSizeBits(int bitsX, int bitsZ);
}
