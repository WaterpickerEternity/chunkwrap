package org.waterpicker.chunkwrap.worldgen.biomegenlayer.tileablelayers;

import static org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil.copyNonStaticFieldsByType;
import static org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil.getValue;
import static org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil.setValue;

import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.GenLayerHills;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.GenLayerTileableUtil;
import org.waterpicker.chunkwrap.worldgen.biomegenlayer.TileableGenLayer;
import org.waterpicker.chunkwrap.worldgen.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.util.List;

public class HillsAndMutatedTileable extends GenLayerHills implements TileableGenLayer {

    private TileableGenLayer[] layers = new TileableGenLayer[1];
    private int sizeX;
    private int sizeZ;

    public HillsAndMutatedTileable(GenLayerHills original) {
        super(0, null, null);
        copyNonStaticFieldsByType(GenLayer.class, long.class, original, this);
        copyNonStaticFieldsByType(GenLayer.class, GenLayer.class, original, this);
        copyNonStaticFieldsByType(GenLayerHills.class, GenLayer.class, original, this);

        this.parent = GenLayerTileableUtil.getTileableLayer(this.parent);

        List<Field> genLayers = ReflectionUtil.getFieldsByType(GenLayer.class, GenLayerHills.class, false);
        assert genLayers.size() == layers.length;
        int i = 0;
        for (Field field : genLayers) {
            GenLayer layer = getValue(field, this, GenLayer.class);
            GenLayer newLayer = GenLayerTileableUtil.getTileableLayer(layer);
            setValue(field, this, newLayer);
            layers[i++] = (TileableGenLayer) newLayer;
        }
    }

    public void initChunkSeed(long x, long z) {
        super.initChunkSeed(x & sizeX, z & sizeZ);
    }

    @Override
    public void setSizeBits(int bitsX, int bitsZ) {
        for (TileableGenLayer layer : layers) {
            layer.setSizeBits(bitsX, bitsZ);
        }
        ((TileableGenLayer) this.parent).setSizeBits(bitsX, bitsZ);
        this.sizeX = GenLayerTileableUtil.sizeBitmask(bitsX);
        this.sizeZ = GenLayerTileableUtil.sizeBitmask(bitsZ);
    }
}
