package org.waterpicker.chunkwrap.worldgen.mapgen;

import net.minecraft.world.World;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.MapGenBase;

public class MapGenTileable extends MapGenBase {

    private MapGenBase generator;

    public MapGenTileable(MapGenBase generator) {
        this.generator = generator;
    }

    @Override
    public void generate(World worldIn, int x, int z, ChunkPrimer blocks) {
        MapGenTileableUtil.generateTileable(generator, worldIn, x, z, blocks);
    }
}