package org.waterpicker.chunkwrap;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketWorldSize implements IMessage {

    private int bitsX, bitsZ;

    public PacketWorldSize() {
    }

    public PacketWorldSize(int bitsX, int bitsZ) {
        this.bitsX = bitsX;
        this.bitsZ = bitsZ;
    }

    @Override public void fromBytes(ByteBuf buf) {
        this.bitsX = buf.readUnsignedByte();
        this.bitsZ = buf.readUnsignedByte();
    }

    @Override public void toBytes(ByteBuf buf) {
        buf.writeByte(bitsX);
        buf.writeByte(bitsZ);
    }

    public static class Handler implements IMessageHandler<PacketWorldSize, IMessage> {

        @Override public IMessage onMessage(PacketWorldSize message, MessageContext ctx) {
            setBits(message.bitsX, message.bitsZ);
            return null;
        }

        private void setBits(int bitsX, int bitsZ) {
            // because apparently it *can* be null
            if (Minecraft.getMinecraft().world == null) {
                Minecraft.getMinecraft().addScheduledTask(() -> setBits(bitsX, bitsZ));
                return;
            }
            ((IWrappedWorld) Minecraft.getMinecraft().world).initSizeBits(bitsX, bitsZ);
        }
    }
}
