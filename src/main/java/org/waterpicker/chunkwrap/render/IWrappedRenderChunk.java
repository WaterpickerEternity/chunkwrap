package org.waterpicker.chunkwrap.render;

import net.minecraft.client.renderer.chunk.RenderChunk;

public interface IWrappedRenderChunk {
    void setBaseChunk(RenderChunk base);

    boolean isWrapped();

    RenderChunk getWrapped();
}
