package org.waterpicker.chunkwrap.render;

import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.chunk.ChunkCompileTaskGenerator;
import net.minecraft.client.renderer.chunk.CompiledChunk;
import net.minecraft.client.renderer.chunk.ListedRenderChunk;
import net.minecraft.client.renderer.chunk.RenderChunk;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.world.World;

import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Nullable;

public class WrappedListedRenderChunk extends ListedRenderChunk implements IWrappedRenderChunk {

    private ListedRenderChunk baseChunk;

    public WrappedListedRenderChunk(World worldIn, RenderGlobal renderGlobalIn, int indexIn) {
        super(worldIn, renderGlobalIn, indexIn);
    }

    @Override
    public void setBaseChunk(RenderChunk base) {
        if (base != null) {
            this.compiledChunk = base.compiledChunk;
        } else if (this.baseChunk != null){
            this.compiledChunk = CompiledChunk.DUMMY;
        }
        this.baseChunk = (ListedRenderChunk) base;
    }

    @Override
    public boolean isWrapped() {
        return baseChunk != null;
    }

    @Override
    public ListedRenderChunk getWrapped() {
        return baseChunk;
    }

    @Override
    public int getDisplayList(BlockRenderLayer layer, CompiledChunk chunk) {
        if (baseChunk != null) {
            return baseChunk.getDisplayList(layer, chunk);
        }
        return super.getDisplayList(layer, chunk);
    }

    @Override
    public boolean setFrameIndex(int frameIndexIn) {
        if (baseChunk != null) {
            return baseChunk.setFrameIndex(frameIndexIn);
        }
        return super.setFrameIndex(frameIndexIn);
    }

    @Override
    public VertexBuffer getVertexBufferByLayer(int layer) {
        if (baseChunk != null) {
            return baseChunk.getVertexBufferByLayer(layer);
        }
        return super.getVertexBufferByLayer(layer);
    }

    @Override
    public void resortTransparency(float x, float y, float z, ChunkCompileTaskGenerator generator) {
        if (baseChunk != null) {
            baseChunk.resortTransparency(x, y, z, generator);
            return;
        }
        super.resortTransparency(x, y, z, generator);
    }

    @Override
    public void rebuildChunk(float x, float y, float z, ChunkCompileTaskGenerator generator) {
        if (baseChunk != null) {
            baseChunk.rebuildChunk(x, y, z, generator);
            return;
        }
        super.rebuildChunk(x, y, z, generator);
    }

    @Override
    protected void finishCompileTask() {
        if (baseChunk != null) {
            throw new Error("Impossible");
        }
        super.finishCompileTask();
    }

    @Override
    public ReentrantLock getLockCompileTask() {
        if (baseChunk != null) {
            return baseChunk.getLockCompileTask();
        }
        return super.getLockCompileTask();
    }

    @Override
    public ChunkCompileTaskGenerator makeCompileTaskChunk() {
        if (baseChunk != null) {
            return baseChunk.makeCompileTaskChunk();
        }
        return super.makeCompileTaskChunk();
    }

    @Nullable
    public ChunkCompileTaskGenerator makeCompileTaskTransparency() {
        if (baseChunk != null) {
            return baseChunk.makeCompileTaskTransparency();
        }
        return super.makeCompileTaskTransparency();
    }

    @Override
    public CompiledChunk getCompiledChunk() {
        if (baseChunk != null) {
            return baseChunk.getCompiledChunk();
        }
        return super.getCompiledChunk();
    }

    @Override
    public void setCompiledChunk(CompiledChunk compiledChunkIn) {
        if (baseChunk != null) {
            baseChunk.setCompiledChunk(compiledChunkIn);
            // no return, set this one too
        }
        super.setCompiledChunk(compiledChunkIn);
    }

    @Override
    public void stopCompileTask() {
        if (baseChunk != null) {
            baseChunk.stopCompileTask();
            return;
        }
        super.stopCompileTask();
    }

    @Override
    public void deleteGlResources() {
        if (baseChunk != null) {
            // delete gl resources only once
            return;
        }
        super.deleteGlResources();
    }

    @Override
    public void setNeedsUpdate(boolean immediate) {
        if (baseChunk != null) {
            baseChunk.setNeedsUpdate(immediate);
            return;
        }
        super.setNeedsUpdate(immediate);
    }

    @Override
    public void clearNeedsUpdate() {
        if (baseChunk != null) {
            baseChunk.clearNeedsUpdate();
            return;
        }
        super.clearNeedsUpdate();
    }

    @Override
    public boolean needsUpdate() {
        if (baseChunk != null) {
            return baseChunk.needsUpdate();
        }
        return super.needsUpdate();
    }

    @Override
    public boolean needsImmediateUpdate() {
        if (baseChunk != null) {
            return baseChunk.needsImmediateUpdate();
        }
        return super.needsImmediateUpdate();
    }
}
