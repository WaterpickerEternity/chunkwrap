package org.waterpicker.chunkwrap.util;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.waterpicker.chunkwrap.IWrappedWorld;

public class Coords {
    /*
    int min = 9999999;
for(RenderChunk rc  : renderChunks) {
    if (rc.getPosition().getX() < min) {
        min = rc.getPosition().getX();
    }
}
new Object[]{min, MathHelper.floor((viewEntityX+8)/16)*16 - countChunksX*8 - 8}
    */

    public static int wrapRelativeX(World world, int pos, int start) {
        return Coords.wrapBlockX(world, pos - start) + start;
    }

    public static int wrapRelativeZ(World world, int pos, int start) {
        return Coords.wrapBlockZ(world, pos - start) + start;
    }

    public static BlockPos wrapBlockPos(World world, BlockPos pos) {
        return new BlockPos(wrapBlockX(world, pos.getX()), pos.getY(), wrapBlockZ(world, pos.getZ()));
    }

    public static double wrapEntityX(World world, double x) {
        int intPart = MathHelper.floor(x);
        double fractPart = x - intPart;
        return wrapBlockX(world, intPart) + fractPart;
    }

    public static double wrapEntityZ(World world, double z) {
        int intPart = MathHelper.floor(z);
        double fractPart = z - intPart;
        return wrapBlockZ(world, intPart) + fractPart;
    }

    // world argument because it will likely depend on dimension in the future
    public static int wrapBlockX(World world, int x) {
        return x & (((IWrappedWorld) world).getSizeXBlocks() - 1);
    }

    public static int wrapBlockZ(World world, int z) {
        return z & (((IWrappedWorld) world).getSizeZBlocks() - 1);
    }

    public static int wrapChunkX(World world, int x) {
        return x & (((IWrappedWorld) world).getSizeXChunks() - 1);
    }

    public static int wrapChunkZ(World world, int z) {
        return z & (((IWrappedWorld) world).getSizeZChunks() - 1);
    }

    // general coords operations

    public static BlockPos midPos(BlockPos p1, BlockPos p2) {
        //bitshift instead of / - round always down
        return new BlockPos((p1.getX() + p2.getX()) >> 1, (p1.getY() + p2.getY()) >> 1, (p1.getZ() + p2.getZ()) >> 1);
    }

    public static int blockToLocal(int val) {
        return val & 0xf;
    }

    public static int blockToChunk(int val) {
        return val >> 4;
    }

    public static int blockCeilToChunk(int val) {
        return -((-val) >> 4);
    }

    public static int blockToBiome(int val) {
        return (val & 14) >> 1;
    }

    public static int localToBlock(int ChunkVal, int localVal) {
        return ChunkToMinBlock(ChunkVal) + localVal;
    }

    public static int ChunkToMinBlock(int val) {
        return val << 4;
    }

    public static int ChunkToMaxBlock(int val) {
        return ChunkToMinBlock(val) + 15;
    }

    public static int getChunkXForEntity(Entity entity) {
        return blockToChunk(MathHelper.floor(entity.posX));
    }

    public static int getChunkZForEntity(Entity entity) {
        return blockToChunk(MathHelper.floor(entity.posZ));
    }

    public static int getChunkYForEntity(Entity entity) {
        // the entity is in the Chunk it's inside, not the Chunk it's standing on
        return blockToChunk(MathHelper.floor(entity.posY));
    }

    public static int blockToChunk(double blockPos) {
        return blockToChunk(MathHelper.floor(blockPos));
    }

    public static int chunkToCenterBlock(int ChunkVal) {
        return localToBlock(ChunkVal, 8);
    }
}